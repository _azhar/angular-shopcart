var productController = angular.module('productController',[]);

productController.controller('productListCtrl', ['$scope', '$http',function($scope, $http) {
  $http.get('thing.json').success(function(data) {
    $scope.products = data;
  });

  $scope.cartCount = 0;
  $scope.cartItems = [];
  $scope.showModal = false;
  $scope.totalCost = 0;
  $scope.totalTax  = 0;

  $scope.addToCart = function(product){
    var no_duplicate = true;
    $scope.cartItems.forEach(function (item, indeks) {
      if (item.id == product.id) {
        no_duplicate = false;
      }
    });
    if (no_duplicate) {
      $scope.cartCount++;
      $scope.totalCost += product.price;
      product.cart_id = $scope.cartCount;
      $scope.cartItems.push(product);
      console.log($scope.cartItems);
      add_tax(product);
    }
  };

  $scope.removeFromCart = function(id){
    if($scope.cartCount > 0){
      $scope.cartCount--;
      $scope.products.forEach(function (item, indeks) {
        if (item.id == id) {
          $scope.totalCost -= $scope.products[indeks].price;
        }
      });
      $scope.cartItems.forEach(function (item, indeks) {
        if (item.id == id) {
          $scope.cartItems.splice(indeks, 1);
          remove_tax(item);
        }
      });
    }
  };

  function add_tax(product) {
      if (product.import == true) {
          var tax = 10/100 * product.price;
          $scope.totalTax += tax;
      } else {
        if ((product.category !== "book") &&
            (product.category !== "medical") &&
            (product.category !== "food")) {
              var tax = 10/100 * product.price;
              $scope.totalTax += tax;
        }
      }
  }

  function remove_tax(product) {
      if (product.import) {
          var tax = 10/100 * product.price;
          $scope.totalTax -= tax;
      } else {
        if ((product.category != "book") &&
            (product.category != "medical") &&
            (product.category != "food") ) {
              var tax = 10/100 * product.price;
              $scope.totalTax -= tax;
        }
      }
  }

}]);
